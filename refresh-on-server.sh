#!/bin/bash
ssh pilgr@pilgr << EOF
cd /home/pilgr/dev/pilgr.com
git pull origin master
source env/bin/activate
make html
EOF
