Title: piHotKeys
Date: 2014-07-19
Category: piHotKeys

**piHotKeys** is PC touch keyboard (either wireless or USB). The keyboard layout is changed automatically and depends on active PC application. For example: if you use Photoshop – it shows instruments; if you use Google Chrome, it gives access to useful key combinations to work efficiently with tabs, history, downloads; if media player is active, – then you have access to the play/pause keys and others. There are 15 applications supported up to date.

You can use either Wi-Fi or USB to get an access to the managed PC. It is important to note that application piServer should be installed on the computer before using it. You can easily download the app here.

**Download [piHotKeys] for Android**.

**Download [piServer] for PC**.

This is a must-have app to control your PC with Android’s touchscreen using hot keys. No more *“Ctrl-C”*, *“Ctrl-Shift-Z”* or tons of other shortcuts to remember — just touch the button on the screen!

Features:

 - connection via WiFi or USB
 - available on-screen hotkeys depend on the active PC application
 - gestures support (switch windows, scroll, Enter, etc)
 - use your Android’s volume hardware buttons to control your PC
 - optimized for ICS
 - app looks amazing both on Android phones and tablets

Useful tips:

 - Double tap for “enter”
 - With one finger on the screen tap with the other to switch between applications
 - Scroll the screen up/down with two fingers to scroll the active page on your PC

Hot keys support for:

 - Photoshop
 - PowerPoint
 - Google Chrome, Firefox, Internet Explorer
 - VLC, Winamp, Windows Media Player, SPlayer, AIMP, KMPlayer, Media Player Classic
 - IntelliJ IDEA, Eclipse
 - Skype
 - Far Manager

OS on PC side: **Windows (XP, Vista, 7) 32bit**

[piHotKeys]:https://play.google.com/store/apps/details?id=name.pilgr.android.picat
[piServer]:/uploads/piServerSetup_2.exe
